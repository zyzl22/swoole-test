<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function(){
    // excel
        Route::prefix('supplier')->group(function ($route) {
            $route->post('index', 'SupplierController@index'); //列表
            $route->post('show', 'SupplierController@show'); //详情
            $route->post('create', 'SupplierController@create'); //新增
            $route->post('store', 'SupplierController@store'); //新增-保存
            $route->post('edit', 'SupplierController@edit'); //编辑
            $route->post('update', 'SupplierController@update'); //编辑-保存
            $route->post('destroy', 'SupplierController@destroy'); //删除
            $route->post('restore', 'SupplierController@restore');
            $route->post('forceDelete', 'SupplierController@forceDelete');
        });
})