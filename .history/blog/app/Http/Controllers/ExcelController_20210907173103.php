<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request)
    {
        try {

        } catch (\Exception $e) {
            $data = [
                'code' => $e->getCode() ? $e->getCode() : Response::HTTP_BAD_REQUEST,
                'msg'  => config('app.env') != 'production' ? $e->getMessage() . ' at line ' . $e->getLine() : $e->getMessage(),
            ];

            return responseJson($data);
        }


        // 指定reader
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($_FILES['file']['tmp_name']);

        return $spreadsheet;
    }

}
