<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request)
    {
        try {

            dd($_FILES);

            // 指定reader
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load($_FILES['file']['tmp_name']);

            return $spreadsheet;
        } catch (\Exception $e) {
            $data = [
                'code' => $e->getCode() ? $e->getCode() : Response::HTTP_BAD_REQUEST,
                'msg' => config('app.env') != 'production' ? $e->getMessage() . ' at line ' . $e->getLine() : $e->getMessage(),
            ];

            return responseJson($data);
        }

    }

}
