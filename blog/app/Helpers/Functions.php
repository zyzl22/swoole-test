<?php
// +----------------------------------------------------------------------
// | User: zq
// +----------------------------------------------------------------------
// | 2021-09-07 17:30:53
// +----------------------------------------------------------------------
use Symfony\Component\HttpFoundation\Response;

if (!function_exists('responseJson')) {
    function responseJson($data, $code = Response::HTTP_OK, $msg = 'success')
    {
        $response = [];

        if (isset($data['code'])) {
            $response['code'] = $data['code'];
            unset($data['code']);
        } else {
            $response['code'] = $code;
        }

        if (isset($data['msg'])) {
            $response['msg'] = $data['msg'];
            unset($data['msg']);
        } else {
            $response['msg'] = $msg;
        }

        $response['data'] = [];
        if ($data) {
            $response['data'] = $data;
        }

        return response()->json($response);
    }
}